# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# c=Comment.create ([{body: 'Comentari 1' }, { body: 'Comentari 2' }])
# p=Post.create({title: '1 post', body: "presentacio 1er post"})
# p.comments <<  c
# puts p.comments

# Post.create([{title: '1 post', body: "presentacio 1er post"}, {title: '2 post', body: "presentacio 2er post"}])

# { post_id: 1, body: 'Comenari 3' }, { post_id: 1,body: 'Comenari 4' }])
